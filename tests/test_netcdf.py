from datetime import datetime

from collections import OrderedDict
from path import Path

import dypy.netcdf as dn

testdatadir = Path.dirname(Path(__file__)).joinpath('test_data')
netcdffile = testdatadir / 'lsl_lagranto2_0.nc'

startdate = datetime(2000, 10, 14, 6)


def test_read_dimensions():
    ori_dims = OrderedDict((('ntra', 900), ('ntim', 31)))
    dims = dn.read_dimensions(netcdffile)
    assert ori_dims == dims


def test_read_variables():
    ori_variables = ['time', 'lon', 'lat', 'z', 'QV']
    variables = dn.read_variables(netcdffile)
    assert ori_variables == variables


def test_read_gattribues():
    ori_gatts = OrderedDict([('ref_year', 2000), ('ref_month', 10),
                             ('ref_day', 14), ('ref_hour', 6),
                             ('ref_min', 0), ('duration', -1800),
                             ('pollon', 0.0), ('pollat', 90.0)])
    gatts = dn.read_gattributes(netcdffile)
    assert ori_gatts == gatts
