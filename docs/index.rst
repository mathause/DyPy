.. _contents:


Welcome to DyPy's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   tutorial

Installation
------------

Using pip
~~~~~~~~~

Ideally install it in a virtual environment. See for example venv_.

.. code:: bash

    pip install git+https://git.iac.ethz.ch/atmosdyn/DyPy.git --process-dependency-links

Then you can install the dependencies for testing or the documentations as follow:

.. code:: bash

    pip install DyPy[testing]
    pip install DyPy[docs]

To build the documentation do the following:

.. code:: bash

    cd /path/to/dypy/location
    cd docs
    make html

To test the package do the following:

.. code:: bash

    cd /path/to/dpyp/location
    pytest

.. _venv: https://docs.python.org/3/library/venv.html#module-venv


.. role:: bash(code)
   :language: bash


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

