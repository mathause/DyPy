Introduction
=============

The aim of the DyPy package is to wrap tools used daily in the group.
The package is divided in several subpackages.

.. toctree::
   :maxdepth: 2
   :glob:

   lagranto
   netcdf
   plotting
   small_tools
   soundings
   constants
   intergrid
   tools